package com.cgi;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Date {

	public static void main(String[] args) {
		Date date = new Date();
		Scanner in = new Scanner(System.in);
		System.out.print("Enter month and year: MM yyyy \n");
		int month = in.nextInt();
		int year = in.nextInt();
		in.close(); 
		
		//HERE I GIVE SOME WORTS (GITLAB)
	
		// VALIDACIA
		try {

			if (month < 1 || month > 12)
				throw new Exception("Invalid index for month: " + month);
			printCalendarMonthYear(month, year);
		}

		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		date.counting();
	}

	private static void printCalendarMonthYear(int month, int year) {

		Calendar cal = new GregorianCalendar();
		cal.clear();
		cal.set(year, month - 1, 1); // nastavenie kalendara

		System.out.println("Calendar for " + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US) + " "
				+ cal.get(Calendar.YEAR)); // vypis kalendara podla mesiaca a roku

		int firstWeekdayOfMonth = cal.get(Calendar.DAY_OF_WEEK);// ktory den je prvy na kalendaru
		int numberOfMonthDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH); // pocet dni v danom mesiaci
		printCalendar(numberOfMonthDays, firstWeekdayOfMonth);
	}
	
	private void counting() {

		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		executor.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				System.out.print("\r Actual time: "
						+ LocalTime.now().truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME)); //LocalTime len JDK 1.8
			}
		}, 0, 1, TimeUnit.SECONDS);

		if (executor == null) {
			executor.shutdown();
		}
	}

	private static void printCalendar(int numberOfMonthDays, int firstWeekdayOfMonth) {
		int weekdayIndex = 0;
		System.out.println("Su  Mo  Tu  We  Th  Fr  Sa");

		for (int day = 1; day < firstWeekdayOfMonth; day++) {
			System.out.print("    "); // aby prvy den bol na spravnom mieste
			weekdayIndex++;
		}

		for (int day = 1; day <= numberOfMonthDays; day++) {

			// DESIGN na jedno a dvojciferne cisla
			if (day < 10)
				System.out.print(day + " ");
			else
				System.out.print(day);
			weekdayIndex++;
			if (weekdayIndex == 7) {
				weekdayIndex = 0;
				System.out.println();
			} else {
				System.out.print("  ");
			}
		}
	}
}